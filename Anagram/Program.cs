﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anagram
{
    class Program
    {
        public static List<String> SortedSolution(String filename)
        {
            List<String> anagrams = new List<String>();
            Dictionary<String, List<String>> sortedWords = new Dictionary<String, List<String>>();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            foreach (String w in File.ReadAllLines(filename))
            {
                char[] chars = w.ToCharArray();
                Array.Sort(chars);
                string s = new String(chars);
                if (sortedWords.ContainsKey(s))
                {
                    sortedWords[s].Add(w);
                }
                else
                {
                    sortedWords.Add(s, new List<String>());
                    sortedWords[s].Add(w);
                }
            }

            foreach (KeyValuePair<String, List<String>> kvp in sortedWords)
            {
                if (kvp.Value.Count > 1)
                    anagrams.Add(String.Join(" ", kvp.Value));
            }

            sw.Stop();
            Console.WriteLine(sw.Elapsed);

            return anagrams;
        }

        //TODO: Improve this by partitioning the data more effectively
        public static List<String> ParallelSortedSolution(String filename)
        {
            List<String> anagrams = new List<String>();
            ConcurrentDictionary<String, ConcurrentQueue<String>> sortedWords = new ConcurrentDictionary<String, ConcurrentQueue<String>>();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            Parallel.ForEach(File.ReadAllLines(filename), w =>
            {
                char[] chars = w.ToCharArray();
                Array.Sort(chars);
                string s = new String(chars);
                if (sortedWords.ContainsKey(s))
                {
                    sortedWords[s].Enqueue(w);
                }
                else
                {
                    if (sortedWords.TryAdd(s, new ConcurrentQueue<String>()))
                        sortedWords[s].Enqueue(w);
                }
            });


            foreach (KeyValuePair<String, ConcurrentQueue<String>> kvp in sortedWords)
            {
                if (kvp.Value.Count > 1)
                    anagrams.Add(String.Join(" ", kvp.Value));
            }

            sw.Stop();
            Console.WriteLine(sw.Elapsed);

            return anagrams;
        }

        //TODO: Implement a Parallel solution using Tasks

        #region LINQ Solutions
        public static List<String> LINQSolution(String filename)
        {
            List<String> anagrams = new List<String>();
            Stopwatch sw = new Stopwatch();
            sw.Start();

            File.ReadAllLines(filename)
                // Create a key value pair, key is sorted characters, value is string array of values with matching key
                .GroupBy(word => new String(word.OrderBy(ch => ch).ToArray())) 
                // Filter the Key Value pairs to those that have more than one value per key
                .Where(matches => matches.Count() > 1).ToList()
                // For each value in the in each of the key values string arrays, 
                // join the values together into a single string and add to list of strings
                .ForEach(matches => anagrams.Add(String.Join(" ", matches)));
                
            sw.Stop();
            Console.WriteLine(sw.Elapsed);

            return anagrams;
        }

        public static List<String> PLINQSolution(String filename)
        {
            List<String> anagrams = new List<String>();
            Stopwatch sw = new Stopwatch();
            sw.Start();

            File.ReadAllLines(filename).AsParallel()
                // Create a key value pair, key is sorted characters, value is string array of values with matching key
                .GroupBy(word => new String(word.OrderBy(ch => ch).ToArray())) 
                // Filter the Key Value pairs to those that have more than one value per key
                .Where(matches => matches.Count() > 1).ToList()
                // For each value in the in each of the key values string arrays, 
                // join the values together into a single string and add to list of strings
                .ForEach(matches => anagrams.Add(String.Join(" ", matches)));
                
            sw.Stop();
            Console.WriteLine(sw.Elapsed);

            return anagrams;
        }
        #endregion

        #region Menu System
        public static void PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1: SortedSoltion()");
            Console.WriteLine("2: LINQSolution()");
            Console.WriteLine("3: ParallelSortedSoltion()");
            Console.WriteLine("4: PLINQSolution()");
            Console.WriteLine("P: Print Last Solution");
            Console.WriteLine("Q: Quit");
        }

        static void Main(string[] args)
        {
            List<String> anagrams = new List<String>();
            ConsoleKeyInfo c;
            String filename = "wordlist.txt";

            PrintMenu();

            while ((c = Console.ReadKey(true)) != null)
            {
                switch (Char.ToLower(c.KeyChar))
                {
                    case '1':
                        anagrams = SortedSolution(filename);
                        break;
                    case '2':
                        anagrams = LINQSolution(filename);
                        break;
                    case '3':
                        anagrams = ParallelSortedSolution(filename);
                        break;
                    case '4':
                        anagrams = PLINQSolution(filename);
                        break;
                    case 'p':
                        PrintAnagrams(anagrams);
                        break;
                    case 'q':
                        return;
                }

                PrintMenu();
            }
        }
        #endregion

        public static void PrintAnagrams(List<String> anagrams)
        {
            foreach (String s in anagrams)
            {
                Console.WriteLine(s);
            }
        }
    }
}
